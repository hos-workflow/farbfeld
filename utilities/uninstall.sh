#!/bin/sh

prefix="${1}"
shift

for i in ${@}; do
	rm -f "${prefix}/${i}"
done
