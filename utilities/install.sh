#!/bin/sh

prefix="${1}"
shift

for i in ${@}; do
	cp -f ${i} "${prefix}"
    chmod 755 "${prefix}/${i}"
done
